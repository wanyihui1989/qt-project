#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QTimer>
#include <QTime>
#include <QDebug>
#include <QFile>
#include <QPushButton>
#include "masklabel.h"



namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    QTimer* mytime;
    QVector<QString> name_items;
    QVector<MaskLabel*> label_items;
    QVector<QPushButton*> btn_items;
    void msleep(int msec);
    ~Widget();

public slots:
    void slot_timeout();


private slots:
    void slot_btn_click();


private:
    Ui::Widget *ui;
};

#endif // WIDGET_H
