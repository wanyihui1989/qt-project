#include "masklabel.h"

MaskLabel::MaskLabel(int pos_x,QString fans_name,QWidget*parent)
    :QLabel(parent)
{
    this->setAlignment(Qt::AlignHCenter);//设置字体居中
    this->setStyleSheet("color:white;font-size:20px");//设置字体颜色大小

    /*粉丝名称移动的动画*/
    animation_fans = new QPropertyAnimation(this,"pos");
    animation_fans->setStartValue(QPoint(pos_x,900));//起始位置
    animation_fans->setEndValue(QPoint(pos_x,-50));//结束位置
    animation_fans->setDirection(QAbstractAnimation::Direction::Forward);
    animation_fans->setDuration(15000);//时长15妙
    animation_fans->start(QAbstractAnimation::DeleteWhenStopped);//动画结束后自动关闭，释放内存


    /*粉丝名称的长宽*/
    this->setText("粉丝名称的长度设置初始化");
    this->adjustSize();//自适应文字的长度
    this->setFixedHeight(50);//设置高度

    this->setText(fans_name);//导入粉丝的名称

    /*粉丝名称的出场渐变效果*/
    QGraphicsOpacityEffect* pGra = new QGraphicsOpacityEffect(this);
    pGra->setOpacity(0);
    this->setGraphicsEffect(pGra);
    QPropertyAnimation* animation_opa = new QPropertyAnimation(pGra,"opacity");
    animation_opa->setDuration(2000);
    animation_opa->setStartValue(0);
    animation_opa->setEndValue(1);
    animation_opa->start(QAbstractAnimation::DeleteWhenStopped);

}


MaskLabel::~MaskLabel()
{

}

